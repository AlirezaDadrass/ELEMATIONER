import { IndexUI } from './Controllers/Main/indexUI';
import elemationerCore from './Controllers/System/Core';

export class Index {
    constructor() {
        this.UIInit();
    }
    UIInit() {
        const indexUI = new IndexUI();
        indexUI.GenerateUI();
      
    }
    CoreTasks(Task, Params) {
        switch (Task) {
            case 'NewWindow': {
                elemationerCore.NewWindow(Params.UIPath, Params.Options)
            } break;
        }
    }

}
const index = new Index()