import * as $ from 'jquery';

import * as electron from 'electron';
const remote = electron.remote;
export class IndexUI {
    constructor() { }
    GenerateUI() {

        document.getElementById("close-btn").addEventListener("click", function (e) {
            var window = remote.getCurrentWindow();
            window.close();
        });
        document.getElementById("min-btn").addEventListener("click", function (e) {
            var window = remote.getCurrentWindow();
            window.minimize();
        });
        document.getElementById("max-btn").addEventListener("click", function (e) {
            var window = remote.getCurrentWindow();
            if (!window.isMaximized()) {
                window.maximize();
            } else {
                window.unmaximize();
            }
        });
        $(document).ready(function () {
            const config = require('../../../config.json')
            $('.toolbar-faveicon').attr('src', config.window_config.icon.replace("./src/app/", "./"));

            var close_hovering = $('.close-hovering');
            var minimize_hovering = $('.minimize-hovering');
            var maximize_hovering = $('.maximize-hovering');
            var mouseOvered = false;
            /* close button efects */
            close_hovering.mouseenter(function () {
                mouseOvered = true;
                close_hovering.removeClass('toolbar-button grey lighten-4');
                close_hovering.addClass('btn-small toolbar-button-hovering red');
                minimize_hovering.css("opacity", "0.1");
                maximize_hovering.css("opacity", "0.1");
                $('#close').removeClass("nav-icon-hidden");
            })
            close_hovering.mouseout(function () {
                mouseOvered = false;
                close_hovering.addClass('toolbar-button grey lighten-4');
                close_hovering.removeClass('btn-small toolbar-button-hovering red');
                minimize_hovering.css("opacity", "1");
                maximize_hovering.css("opacity", "1");
                $('#close').addClass("nav-icon-hidden");
            })
            /* /close button efects */
            /* minimize button efects */
            minimize_hovering.mouseenter(function () {
                mouseOvered = true;
                minimize_hovering.removeClass('toolbar-button grey lighten-4');
                minimize_hovering.addClass('btn-small toolbar-button-hovering blue');
                close_hovering.css("opacity", "0.1");
                maximize_hovering.css("opacity", "0.1");
                $('#mini').removeClass("nav-icon-hidden");
            })
            minimize_hovering.mouseout(function () {
                mouseOvered = false;
                minimize_hovering.addClass('toolbar-button grey lighten-4');
                minimize_hovering.removeClass('btn-small toolbar-button-hovering blue');
                close_hovering.css("opacity", "1");
                maximize_hovering.css("opacity", "1");
                $('#mini').addClass("nav-icon-hidden");
            })
            /* /minimize button efects */
            /* maximaiz button efects */
            maximize_hovering.mouseenter(function () {

                maximize_hovering.removeClass('toolbar-button grey lighten-4');
                maximize_hovering.addClass('btn-small toolbar-button-hovering blue');
                close_hovering.css("opacity", "0.1");
                minimize_hovering.css("opacity", "0.1");
                $('#maxi').removeClass("nav-icon-hidden");
            })
            maximize_hovering.mouseout(function () {
                mouseOvered = false;
                maximize_hovering.addClass('toolbar-button grey lighten-4');
                maximize_hovering.removeClass('btn-small toolbar-button-hovering blue');
                close_hovering.css("opacity", "1");
                minimize_hovering.css("opacity", "1");
                $('#maxi').addClass("nav-icon-hidden");
            })
            /* /maximaiz button efects */
            /* toolbar efects */

            setInterval(function () {
                if (!mouseOvered) {
                    close_hovering.removeClass('grey lighten-4');
                    close_hovering.addClass('red lighten-2');
                    setTimeout(function () {
                        close_hovering.removeClass('red lighten-2');
                        close_hovering.addClass('grey lighten-4');
                        setTimeout(function () {
                            maximize_hovering.removeClass('grey lighten-4');
                            maximize_hovering.addClass('blue lighten-2');
                            setTimeout(function () {
                                maximize_hovering.removeClass('blue lighten-2');
                                maximize_hovering.addClass('grey lighten-4');
                                setTimeout(function () {
                                    minimize_hovering.removeClass('grey lighten-4');
                                    minimize_hovering.addClass('blue lighten-2');
                                    setTimeout(function () {
                                        minimize_hovering.removeClass('blue lighten-2');
                                        minimize_hovering.addClass('grey lighten-4');
                                    }, 500)
                                }, 500)
                            }, 500)
                        }, 500)
                    }, 500)
                }
            }, 5000)

            /* /toolbar efects */

            function mainContainerSizer() {
                var w = window.innerWidth;
                var h = window.innerHeight;
                $('.main-container').css('height', h - 75);
                $('.main-container').css('width', w);
            }
            mainContainerSizer()
            $(window).resize(function () { mainContainerSizer() });
        })
    }
}
