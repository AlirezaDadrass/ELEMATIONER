const gulp = require('gulp');
const ts = require('gulp-typescript');
const allFiles = [
  'src/*.css', 'src/**/*.css',
  'src/*.js', 'src/**/*.js',
  'src/*.png', 'src/**/*.png',
  'src/*.jpg', 'src/**/*.jpg',
  'src/*.ico', 'src/**/*.ico',
  'src/*.eot', 'src/**/*.eot',
  'src/*.svg', 'src/**/*.svg',
  'src/*.ttf', 'src/**/*.ttf',
  'src/*.woff', 'src/**/*.woff',
  'src/*.woff2', 'src/**/*.woff2',
  'src/*.json', 'src/**/*.json',
  'src/*.html', 'src/**/*.html'  
];

// pull in the project TypeScript config
const tsProject = ts.createProject('tsconfig.json');

gulp.task('scripts', () => {
  const tsResult = tsProject.src()
    .pipe(tsProject());
  return tsResult.js.pipe(gulp.dest('Edist'));
});
gulp.task('all', function () {
  return gulp.src(allFiles)
    .pipe(gulp.dest('Edist'));

});

gulp.task('watch', ['scripts', 'all'], () => {
  gulp.watch('src/**/*.ts', ['scripts']);
  gulp.watch('src/**/*.*', ['all']);

});

gulp.task('assets', function () {
  return gulp.src(allFiles)
    .pipe(gulp.dest('Edist'));
});

gulp.task('default', ['watch', 'assets']);