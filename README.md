# Electron Materialize Application Builder
# ELEMATIONER!

"npm i" install all modules<br />
"electron-builder install-app-dep" electron builder dependencies installer<br />
"npm run x86" build ia32 architecture for windows<br />
"npm run x64" build x64 architecture for windows<br />

Projetc Tree:<br />
```
.
├── src
|  ├── Core
|  |  ├── Controllers
|  |  |  └── <Contollers modules>
|  |  └── Database
|  |     └── <Database connections and modules>
|  ├── UI
|  |  ├── Components
|  |  |  ├── CSS
|  |  |  ├── Fonts
|  |  |  ├── Icons
|  |  |  ├── Images
|  |  |  └── JS
|  |  └── Views
|  |     └── <Window UI views>
|  |
|  ├── config.json <Main app script config>
|  └── elemationer.js <Main app script>
|
├── .gitignore
├── package-lock.json
├── package.json
└── README.md
```